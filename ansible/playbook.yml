---
- name: Basic deploy of a service
  hosts: localhost
  gather_facts: true

  vars:
    project_dir: "$CI_PROJECT_DIR/terraform/"

  tasks:
    - name: Deploy service with Terraform
      community.general.terraform:
        project_path: '{{ project_dir }}'
        state: present
      register: terraform_output

    - name: Register instance_ips output
      ansible.builtin.set_fact:
        instance_ips: '{{ terraform_output.outputs.instance_ips.value | first }}'

    - name: Echo test IP
      ansible.builtin.debug:
        var: instance_ips

    - name: Debug Inventory after Terraform
      ansible.builtin.debug:
        var: hostvars

    - name: Echo Operating System
      ansible.builtin.debug:
        var: ansible_distribution

    - name: Refresh inventory
      ansible.builtin.meta: refresh_inventory

    - name: Debug Available Hosts
      ansible.builtin.debug:
        var: groups['tag_Name_Web_Server']

    - name: Debug instance_ips
      ansible.builtin.debug:
        var: instance_ips

    - name: Wait for server to be reachable on port 22
      ansible.builtin.wait_for:
        host: "{{ instance_ips }}"
        port: 22
        delay: "{{ check_delay | default(10) }}"
        timeout: "{{ check_timeout | default(300) }}"
      register: ssh_check

#    - name: Pause for 90 seconds
#      pause:
#        seconds: 90

    - name: Debug SSH check result
      ansible.builtin.debug:
        var: ssh_check

- name: Tasks on ec2 instance
  hosts: tag_Name_Web_Server
  gather_facts: true

  tasks:
    - name: Update packages
      become: true
      ansible.builtin.apt:
        update_cache: true

    - name: Install Docker prerequisites
      become: true
      ansible.builtin.apt:
        name: "{{ item }}"
        state: present
      loop:
        - apt-transport-https
        - ca-certificates
        - curl
        - software-properties-common

    - name: Add Docker GPG key
      become: true
      ansible.builtin.apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg
        state: present

    - name: Add Docker repository
      become: true
      ansible.builtin.apt_repository:
        repo: 'deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ ansible_distribution_release }} stable'
        state: present

    - name: Install Docker
      become: true
      ansible.builtin.apt:
        name: docker-ce
        state: present

    - name: Install pip for python3
      ansible.builtin.apt:
        name: python3-pip
        state: present
      become: true

    - name: Create directory for Docker CLI plugins
      ansible.builtin.file:
        path: "~/.docker/cli-plugins/"
        state: directory
        mode: '0755'

    - name: Download Docker Compose binary
      ansible.builtin.get_url:
        url: "https://github.com/docker/compose/releases/download/v2.3.3/docker-compose-linux-x86_64"
        dest: "~/.docker/cli-plugins/docker-compose"
        mode: '0755'

    - name: Verify Docker Compose installation
      ansible.builtin.command: docker compose version
      register: compose_version_output

    - name: Start and enable Docker service
      become: true
      ansible.builtin.service:
        name: docker
        state: started
        enabled: true

    - name: Check Docker version
      ansible.builtin.command: "docker --version"
      register: docker_version
      changed_when: false

    - name: Check Docker Compose version
      ansible.builtin.command: "docker compose version"
      register: docker_compose_version
      changed_when: false

    - name: Display Docker and Docker Compose versions
      ansible.builtin.debug:
        var: item.stdout
      loop:
        - "{{ docker_version }}"
        - "{{ docker_compose_version }}"

    - name: Create /docker directory on remote host
      ansible.builtin.file:
        path: /docker
        state: directory
        mode: '0755'
      become: true

    - name: Copy docker-compose.yaml from local to remote
      ansible.builtin.copy:
        src: ./docker/docker-compose.yaml
        dest: /docker/docker-compose.yaml
        remote_src: false
        mode: '0755'
      become: true
      register: copy_result

    - name: Display copy result
      ansible.builtin.debug:
        var: copy_result

    - name: Install python3-docker package
      ansible.builtin.apt:
        name: python3-docker
        state: present
      become: true

    - name: Install docker-compose Python module
      ansible.builtin.pip:
        name: docker-compose
        executable: pip3
      become: true

    - name: Run docker-compose up on remote
      community.general.docker_compose:
        project_src: /docker
        state: present
      become: true
      register: compose_result

    - name: Display state of litespeed-container
      ansible.builtin.debug:
        var: compose_result.services['litespeed-container']['lite']['state']

    - name: Display state of nginx-proxy
      ansible.builtin.debug:
        var: compose_result.services['nginx-proxy']['nginx']['state']
