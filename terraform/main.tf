terraform {
  required_version = ">= 0.13"

  backend "http" {}
}

provider "aws" {
}

resource "aws_security_group" "web" {
  name        = "web_security_group_new"
  description = "Security group for web server"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 75
    to_port     = 75
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web_server" {
  ami             = "ami-06dd92ecc74fdfb36"
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.web.name]
  key_name        = "web_project"
  tags = {
    Name        = "Web Server"
    Environment = "Production"
    Project     = "MyProject"
  }
}

output "instance_ips" {
  value = aws_instance.web_server[*].public_ip
}


